#mock-mail-server
模拟邮件服务器，用来方便测试发邮件功能。

测试时，业务系统通过http协议发邮件到模拟邮件服务器，不再发送到真实的邮件服务器。

1. 方便查看他人收到的邮件，而不用担心帐号问题。
2. 方便实现自动化测试，验证邮件内容 。

采用spring boot，mybatis，h2，jquery实现

# 使用
```
mvn clean install

java -jar mockmailserver-1.0.jar

访问应用 http://localhost:9000
```

# 效果图
![multisources](img/demo.jpg)

# 客户端如何发邮件
客户端发送http post请求，请求地址/mail/send，数据格式
```
{
    "from": "user@126.com",
    "to": [
        "to1@126.com",
        "to2@126.com"
    ],
    "subject": "邮件主题",
    "content": "<html>邮件内容,<button>按钮</button><html>"
}
```

# JAVA客户端示例
```
package jcode.project.mailserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;


public class HtmlEmailDemo {

    public static class Email {

        private String from;
        private String[] to;
        private String subject;
        private String content;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String[] getTo() {
            return to;
        }

        public void setTo(String[] to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static void main(String[] args) throws Exception {
        Email mail = new Email();
        mail.setContent("<html>邮件内容,<button>按钮</button><html>");
        mail.setSubject("邮件主题");
        mail.setFrom("user@126.com");
        mail.setTo(new String[]{"to1@126.com", "to2@126.com"});

        String result = Unirest.post("http://localhost:9000/mail/send")
                .header("Content-type", "application/json; charset=utf-8")
                .body(new ObjectMapper().writeValueAsString(mail))
                .asString()
                .getBody();
        System.out.println(result);

    }

}
```

