package jcode.project.mailserver.entity;

import jcode.project.base.JSONUtils;

public class Mail {

    private Integer id;
    private String subject;
    private String data;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public HtmlEmail toHtmlEmail() {
        return JSONUtils.toObject(this.data, HtmlEmail.class);
    }

    public void store(HtmlEmail htmlEmail) {
        this.subject = htmlEmail.getSubject();
        this.data = JSONUtils.toJSON(htmlEmail);
    }

}
